#!/usr/bin/env lua
if tonumber((string.gsub(_VERSION, "Lua ", ""))) < 5.2 then error("Require Lua 5.2"); end
local _FILENAME = "log.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "20170222"
--[[
Copyright (c) MIT-License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

--[[ TEMPLATE to implement log
-- if no logger is provided .. DROP log.write() statements
local log = {write = function(is_level, is_message, ...) end, getFile = function(is_name) return; end}
setLog = function(it_log)
  if it_log then
    log = it_log
  end
end
]]

--[=[
if arg and (arg[1] == "test") then
  arg = nil
  local log = require("log")

  -- Set the message format ${TIMESTAMP|LEVEL|MESSAGE} will be replaced
  local s_message_format = "${TIMESTAMP} - ${LEVEL}: ${MESSAGE}"
  log.setMessageFormat(s_message_format)
  io.write(string.format("setMessageFormat(%s) => %s\n", s_message_format, log.getMessageFormat()))

  -- Set the time format os.date() will be used
  local s_time_format = "TIMESTAMP(%s)=%Y/%m/%d %H:%M"
  log.setTimeFormat(s_time_format)
  io.write(string.format("setTimeFormat(%s) => %s\n", s_time_format, log.getTimeFormat()))

  local s_logfile = "/tmp/test.log"
  log.setFile("TEST", s_logfile)
  log.setFile({"TEST1", "TEST2"}, s_logfile)
  log.write("TEST", "Test log - line should be removed..")
  log.write("TEST", false)
  local s_logtest = "Test log"
  log.write("TEST", s_logtest)
  io.write(string.format("log.write(\"%s\") => \"%s\"", s_logtest, io.open(s_logfile, "r"):read("*a")))
  os.execute("rm " .. s_logfile)

  io.write("\n\nstdout/stderr:\n")
  log.setFile({TEST3 = "stdout", TEST4 = "stderr", TEST5 = "|/bin/cat"})
  log.setMessageFormat("${MESSAGE}\n")
  log.write("TEST3", "Test stdout\r\n")
  log.write("TEST4", "Test stderr\r\n")
  log.write("TEST5", "Test pipe to /bin/cat\r\n")

  -- remove TEST2
  log.setFile("TEST2")

  io.write("\nlog files (TEST, TEST1, TEST3, TEST4, TEST5): \n")
  for s_level in log.getLevel() do
    io.write(string.format("%s => %s\n", s_level, log.getFile(s_level)))
  end

  io.write("\nnew log object:\n")
  local l = log.newLog()
  l:setFile("TEST", "stdout")
  l:setFile({"TEST2", "TEST3"}, "stdout")
  for s_level in l:getLevel() do
    io.write(string.format("%s => %s\n", s_level, l:getFile(s_level)))
  end
  l:write("TEST", "Message")
end
--]=]

local DEFAULT_TIME_FORMAT = "%Y/%m/%d-%H:%M:%S"
local DEFAULT_MESSAGE_FORMAT = "{\"${TIMESTAMP}\", \"${LEVEL}\", \"${MESSAGE}\"},\n"
local PATTERN_CONTROL_CHAR = "[\r\n\"]"
local REPLACE_CONTROL_CHAR = {
  ["\r"] = "<CR>",
  ["\n"] = "<LF>",
  ["\""] = "\\\""
}

local pairs, pcall, setmetatable, type = pairs, pcall, setmetatable, type
local io = {open = io.open, popen = io.popen, stderr = io.stderr, stdout = io.stdout}
local os = {date = os.date, getenv = os.getenv, time = os.time}
local string = {find = string.find, format = string.format, gsub = string.gsub, sub = string.sub}
local _M = {}
_ENV = nil

_M.getVersion = function() return _VERSION, _AUTHOR, _FILENAME; end

local t_global_log_data = {
  t_logger = {},
  t_format = {
    s_time = DEFAULT_TIME_FORMAT,
    s_message = DEFAULT_MESSAGE_FORMAT,
  },
  t_file = {},
}

local getFormat = function(is_type, it_log_data)
-- returns format
  if type(it_log_data) == "table" then
    return it_log_data._t_log_data.t_format[is_type]
  end
  return t_global_log_data.t_format[is_type]
end

local setFormat = function(is_type, it_log_data, is_format)
-- returns format
  if (type(it_log_data) == "table") and (type(is_format) == "string") then
    it_log_data._t_log_data.t_format[is_type] = is_format
  elseif type(it_log_data) == "string" then
    t_global_log_data.t_format[is_type] = it_log_data
  end
  return getFormat(is_type, it_log_data)
end

_M.getMessageFormat = function(it_log_data)
-- returns message format
  return getFormat("s_message", it_log_data)
end

_M.setMessageFormat = function(it_log_data, is_message_format)
-- returns message format
  return setFormat("s_message", it_log_data, is_message_format)
end

_M.getTimeFormat = function(it_log_data)
-- returns time format
  return getFormat("s_time", it_log_data)
end

_M.setTimeFormat = function(it_log_data, is_time_format)
-- returns time format
  return setFormat("s_time", it_log_data, is_time_format)
end

_M.getLevel = function(it_log_data)
  if it_log_data then
    return pairs(it_log_data._t_log_data.t_file)
  end
  return pairs(t_global_log_data.t_file)
end

_M.getFile = function(it_log_data, is_level)
-- returns log file
  if (type(it_log_data) == "table") and it_log_data._t_log_data then
    return it_log_data._t_log_data.t_file[is_level]
  end
  return t_global_log_data.t_file[it_log_data]
end

_M.write = function(it_log_data, is_level, is_message, ...)
-- no return value
-- if is_message = false reset log file and return true
  local s_log_file_name = _M.getFile(it_log_data, is_level)
  if s_log_file_name then
    local s_message_arg = nil
    if not(type(it_log_data) == "table") then
      s_message_arg = is_message
      is_message = is_level
      is_level = it_log_data
    end
    if type(is_message) == "string" then
      local b_status, s_message = nil, nil
      if s_message_arg then
        b_status, s_message = pcall(string.format, is_message, s_message_arg, ...)
      else
        b_status, s_message = pcall(string.format, is_message, ...)
      end
      if b_status then
        is_message = s_message
      end
    else
      -- reset
      if (type(is_message) == "boolean") and not(is_message) and not(io[s_log_file_name]) then
        local fw_out, s_ioerr = io.open(s_log_file_name, "w")
        if fw_out then
          fw_out:close()
          return
        end
      end
      is_message = ""
    end
    local fa_out = false
    if (string.find(s_log_file_name, "|") == 1) then
      fa_out = io.popen(string.sub(s_log_file_name, 2), "w")
    else
      fa_out = io[s_log_file_name] or io.open(s_log_file_name, "a")
    end
    if fa_out then
      fa_out:write((string.gsub(getFormat("s_message", it_log_data), "%${([^}]*)}", {
        ["LEVEL"] = is_level,
        ["MESSAGE"] = string.gsub(is_message, PATTERN_CONTROL_CHAR, REPLACE_CONTROL_CHAR),
        ["TIMESTAMP"] = os.date((string.gsub(getFormat("s_time", it_log_data), "%%s", os.time()))),
      })))
      fa_out:close()
      return
    end
  end
end

_M.setFile = function(it_log_data, ist_level, is_log_file_name) end
_M.setFile = function(it_log_data, ist_level, is_log_file_name)
  local t_log_data = it_log_data and it_log_data._t_log_data
  if not(type(it_log_data) == "table") or not(it_log_data._t_log_data) then
    is_log_file_name = ist_level
    ist_level = it_log_data
    t_log_data = t_global_log_data
    it_log_data = nil
  end
  if type(ist_level) == "table" then
    for k, v in pairs(ist_level) do
      if type(is_log_file_name) == "string" then
        _M.setFile(it_log_data, v, is_log_file_name)
      else
        if (it_log_data) then
          _M.setFile(it_log_data, k, v)
        else
          _M.setFile(k, v)
        end
      end
    end
    return _M.getLevel(it_log_data)
  elseif ist_level then
    t_log_data.t_file[ist_level] = nil
    t_log_data.t_logger[ist_level] = nil
    if is_log_file_name then
      local fa_out = false
      if (string.find(is_log_file_name, "|") == 1) then
        fa_out = io.popen(string.sub(is_log_file_name, 2), "w")
      else
        fa_out = io[is_log_file_name] or io.open(is_log_file_name, "a")
      end
      if fa_out then
        fa_out:close()
        t_log_data.t_file[ist_level] = is_log_file_name
        t_log_data.t_logger[ist_level] = function(it_log_data, is_message, ...) _M.write(it_log_data, ist_level, is_message, ...); end
      end
    end
    return _M.getFile(it_log_data, ist_level)
  end
end

_M.newLog = function()
-- returns log data object
  local logger = {
    getMessageFormat = _M.getMessageFormat,
    setMessageFormat = _M.setMessageFormat,
    getTimeFormat = _M.getTimeFormat,
    setTimeFormat = _M.setTimeFormat,
    getLevel = _M.getLevel,
    getFile = _M.getFile,
    setFile = _M.setFile,
    write = _M.write,
    _t_log_data = {
      t_logger = {},
      t_format = {
        s_time = DEFAULT_TIME_FORMAT,
        s_message = DEFAULT_MESSAGE_FORMAT,
      },
      t_file = {},
    }
  }
  setmetatable(logger, {__index = function(it, is) return (logger._t_log_data.t_logger[is] or function() end) end})
  for _, s_log_level in pairs({"DEBUG", "INFO", "WARN", "ERROR"}) do
    if os.getenv(string.format("LOG_%s", s_log_level)) then
      logger:setMessageFormat("${LEVEL}\t${MESSAGE}\n")
      logger:setFile(s_log_level, os.getenv(string.format("LOG_%s", s_log_level)))
    end
  end
  return logger
end

return _M

